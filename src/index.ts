import figlet from "figlet";
import {PrismaClient} from "@prisma/client";

const prisma = new PrismaClient();

const server = Bun.serve({
  port: 3000,
  fetch(req) {
    const body = figlet.textSync(" let's play with Bun!");
    return new Response(body);
  },
});
console.log(`Listening on http://localhost:${server.port} ...`);
console.log(Bun.version);


// count the number of users
const count = await prisma.user.count();
console.log(`There are ${count} users in the database.`);