import {PrismaClient} from "@prisma/client";

const prisma = new PrismaClient();
// create a new user
await prisma.user.create({
  data: {
    name: "John Dough",
    email: `john-${Math.random()}@example.com`,
  },
});